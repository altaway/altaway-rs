use std::{
    *,
    collections::*
};

pub struct Scanner {
	buffer: VecDeque<String>
}

impl Scanner {
	/// Buffered input, one variable at a time.
	/// 
	/// # Example
	/// ```
	/// let mut scanner = Scanner::new();
	/// let x: i32 = scanner.next().unwrap();
	/// println!("{}", x);
	/// let y: String = scanner.next().unwrap();
	/// println!("{}", y);
	/// ```
	pub fn next<T: str::FromStr>(&mut self) -> result::Result<T, T::Err> {
		while self.buffer.len() == 0 {
			let mut available = String::new();
			io::stdin().read_line(&mut available).unwrap();
			self.buffer = available.split_whitespace().map(String::from).collect();
		}
		self.buffer.pop_front().unwrap().parse()
	}
}

impl Default for Scanner {
	fn default() -> Self {
		Scanner {
			handle: std::io::stdin(),
			buffer: std::collections::VecDeque::new()
		}		
	}
}

/// In the debug mode, acts like std::dbg.
/// In the release mode, does nothing.
/// 
/// # Example
/// ```
/// dbg!(x, y, z);
/// ```
#[macro_export]
macro_rules! dbg {
	($($x:tt)*) => {
		#[cfg(debug_assertions)] std::dbg!($($x)*)
	}
}